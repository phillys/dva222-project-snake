﻿using System.Drawing;
using Snake.Interfaces;

namespace Snake
{
    class Collider : ICollider
    {
        private int width, height;
        private ICollidable[,] collisionMatrix;

        public Collider(int width, int height)
        {
            collisionMatrix = new ICollidable[width, height];
            this.width = width;
            this.height = height;

            Clear();
        }
        
        public void Clear()
        {
            for (var x = 0; x < width; x++)
            {
                for (var y = 0; y < height; y++)
                {
                    Set(null, x, y);
                }
            }
        }

        public void Collide(ICollidable collidable)
        {
            var existingCollidable = Get(collidable.Position);

            if (existingCollidable == null)
            {
                Set(collidable);
            }
        }

        public void Collide(Snake snake)
        {
            Collide(snake.Head);

            foreach (var block in snake.Blocks)
            {
                Collide(block);
            }
        }

        private void Collide(SnakeHead head)
        {
            if (! IsWithinBounds(head.Position))
            {
                head.Snake.Die();
                return;
            }

            var existingCollidable = Get(head.Position);

            if (existingCollidable == null)
            {
                Set(head);
            }
            else
            {
                existingCollidable.OnCollision(head);
            }
            // More than two collidables on same position
            // Overwrite or Z index IList<ICollidable>[,]?
        }

        private void Collide(SnakeBlock block)
        {
            var existingCollidable = Get(block.Position);

            if (existingCollidable == null)
            {
                Set(block);
            }
            else
            {
                existingCollidable.OnCollision(block);
            }
        }

        private bool IsWithinBounds(Point position)
        {
            return position.X >= 0 &&
                   position.Y >= 0 &&
                   position.X < width &&
                   position.Y < height;
        }

        private ICollidable Get(Point position)
        {
            return collisionMatrix[position.X, position.Y];
        }

        private void Set(ICollidable collidable, int x, int y)
        {
            collisionMatrix[x, y] = collidable;
        }

        private void Set(ICollidable collidable, Point position)
        {
            Set(collidable, position.X, position.Y);
        }

        private void Set(ICollidable collidable)
        {
            Set(collidable, collidable.Position);
        }
    }
}
