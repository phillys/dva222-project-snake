﻿namespace Snake.Interfaces
{
    interface ICollider
    {
        void Clear();
        void Collide(ICollidable collidable);
        void Collide(Snake snake);
    }
}
