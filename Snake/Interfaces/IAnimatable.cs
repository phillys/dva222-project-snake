﻿namespace Snake.Interfaces
{
    interface IAnimatable : IRenderable
    {
        void Tick();
    }
}
