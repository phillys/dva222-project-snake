﻿using System.Drawing;

namespace Snake.Interfaces
{
    interface IRenderer
    {
        void DrawSquare(Point position, Color color);
    }
}
