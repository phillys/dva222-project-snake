﻿using System.Drawing;

namespace Snake.Interfaces
{
    interface ICollidable : IRenderable
    {
        Point Position { get; }
        void OnCollision(SnakeHead head);
        void OnCollision(SnakeBlock block);
    }
}
