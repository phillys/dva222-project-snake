﻿namespace Snake.Interfaces
{
    interface IRenderable
    {
        void Draw(IRenderer renderer);
    }
}
