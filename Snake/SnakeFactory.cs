﻿using System;
using System.Drawing;

namespace Snake
{
    class SnakeFactory
    {
        private Engine game;
        private int counter = 0;
        private Color[] colors = new Color[] { Color.Cyan, Color.Magenta, Color.LightGreen };

        public SnakeFactory(Engine game)
        {
            this.game = game;
        }

        public void Reset()
        {
            counter = 0;
        }

        public Snake Create(int x, int y, string name)
        {
            if (counter > 2) throw new InvalidOperationException("Maximum three players!!!");
            return new Snake(x, y, colors[counter++], name, game);
        }
    }
}
