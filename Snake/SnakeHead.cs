﻿using System.Drawing;
using Snake.Foods;

namespace Snake
{
    class SnakeHead : SnakeBlock
    {
        private Engine game;

        public SnakeHead(int x, int y, Color color, Snake snake, Engine game)
            : base(x, y, color, snake)
        {
            this.game = game;
        }

        public override void OnCollision(SnakeHead other)
        {
            Snake.Die();
            Snake.Score += 5;
            other.Eat(this);
            other.Snake.Score += 5;
        }

        public override void OnCollision(SnakeBlock other)
        {
            Snake.Die();
            if (Snake != other.Snake) other.Snake.Score += 5;
        }

        public void Eat(SnakeHead head)
        {
            Snake.Die();
        }

        public void Eat(SnakeBlock block)
        {
            Snake.Die();
            block.Snake.Score += 5;
        }

        public void Eat(StandardFood food)
        {
            Snake.IncreaseLength();
            Snake.Score += 1;
            food.Eat();
        }

        public void Eat(ValuableFood food)
        {
            Snake.IncreaseLength(2);
            Snake.Score += 5;
            food.Eat();
        }

        public void Eat(DoubleSpeedFood food)
        {
            game.GetRandomSnake().DoubleSpeedFor(10);
            food.Eat();
        }
    }
}
