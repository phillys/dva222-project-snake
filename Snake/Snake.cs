﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Threading.Tasks;
using Snake.Interfaces;

namespace Snake
{
    class Snake : IAnimatable
    {
        private Color color;
        private IList<SnakeBlock> blocks = new List<SnakeBlock>();
        private Directions direction = Directions.Down;
        private Engine game;
        private SnakeHead head;
        private Directions lastDirection = Directions.Down;
        private int lengthToIncrease = 10;
        private const int minSpeed = 10;
        private string name;
        private int score = 0;
        private int speed = 10;
        private Trigger trigger;

        public IList<SnakeBlock> Blocks => new List<SnakeBlock>(blocks);

        public Color Color => color;

        public Directions Direction
        {
            get => direction;
            set
            {
                if (ValidDirection(value))
                {
                    direction = value;
                }
            }
        }

        public SnakeHead Head => head;

        public string Name => name;

        public int Score
        {
            get => score;
            set
            {
                score = value;
                ScoreChanged?.Invoke(name, value);
            }
        }

        public enum Directions
        {
            Up,
            Down,
            Left,
            Right
        }

        public delegate void ScoreChangedHandler(string name, int score);
        public static event ScoreChangedHandler ScoreChanged;

        public Snake(int x, int y, Color color, String name, Engine game)
        {
            this.color = color;
            this.name = name;
            this.game = game;

            trigger = new Trigger(game.FPS / speed);
            trigger.ThresholdReached += OnIncreaseLength;
            trigger.ThresholdReached += OnMove;

            head = new SnakeHead(x, y, color, this, game);
        }

        private void OnIncreaseLength()
        {
            if (lengthToIncrease > 0)
            {
                blocks.Add(new SnakeBlock(blocks.Count == 0 ? head : blocks[blocks.Count - 1]));
                lengthToIncrease--;
            }
        }

        private Directions GetOppositeDirection(Directions direction)
        {
            switch (direction)
            {
                case Directions.Up: return Directions.Down;
                case Directions.Down: return Directions.Up;
                case Directions.Left: return Directions.Right;
                case Directions.Right: return Directions.Left;
                default: throw new ArgumentException();
            };
        }

        private bool ValidDirection(Directions direction)
        {
            return GetOppositeDirection(lastDirection) != direction;
        }

        private void OnMove()
        {
            blocks.Insert(0, new SnakeBlock(head));
            blocks.RemoveAt(blocks.Count - 1);

            switch (direction)
            {
                case Directions.Up:
                    head = new SnakeHead(head.Position.X, head.Position.Y - 1, color, this, game);
                    break;
                case Directions.Down:
                    head = new SnakeHead(head.Position.X, head.Position.Y + 1, color, this, game);
                    break;
                case Directions.Left:
                    head = new SnakeHead(head.Position.X - 1, head.Position.Y, color, this, game);
                    break;
                case Directions.Right:
                    head = new SnakeHead(head.Position.X + 1, head.Position.Y, color, this, game);
                    break;
            }

            lastDirection = direction;
            trigger.Threshold = game.FPS / speed;
        }

        public void Die()
        {
            game.Remove(this);
        }

        private async void HalveSpeedAfter(int seconds)
        {
            await Task.Delay(seconds * 1000);
            if (speed / 2 >= minSpeed) speed /= 2;

            //Console.WriteLine($"[{DateTime.Now}]: ResetSpeed {speed}");
        }

        public void DoubleSpeedFor(int seconds)
        {
            if (speed * 2 <= game.FPS) speed *= 2;

            // Alternative to async approach
            //var timer = new Timer();
            //timer.Interval = seconds * 1000;
            //timer.Start();
            //timer.Tick += delegate (object sender, EventArgs e)
            //{ if (speed / 2 >= minSpeed) speed /= 2; };

            HalveSpeedAfter(seconds);

            //Console.WriteLine($"[{DateTime.Now}]: DoubleSpeedFor {speed}");
        }

        public void Draw(IRenderer renderer)
        {
            head.Draw(renderer);

            foreach(var block in blocks)
            {
                block.Draw(renderer);
            }
        }

        public void IncreaseLength(int length = 1)
        {
            lengthToIncrease += length;
        }

        public void Tick()
        {
            trigger.Tick();
        }
    }
}
