﻿using System.Drawing;
using Snake.Interfaces;

namespace Snake
{
    class SnakeBlock : ICollidable
    {
        private Color color;
        public Point Position { get; private set; }
        public Snake Snake { get; private set; }

        public SnakeBlock(int x, int y, Color color, Snake snake)
        {
            Position = new Point(x, y);
            this.color = color;
            Snake = snake;
        }

        public SnakeBlock(SnakeBlock block)
            : this(block.Position.X, block.Position.Y, block.color, block.Snake) { }

        public void Draw(IRenderer renderer)
        {
            renderer.DrawSquare(Position, color);
        }

        public virtual void OnCollision(SnakeBlock other)
        {
            Snake.Die();
            other.Snake.Die();
        }

        public virtual void OnCollision(SnakeHead other)
        {
            other.Eat(this);
        }
    }
}
