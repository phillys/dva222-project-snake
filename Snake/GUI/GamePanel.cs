﻿using System.Drawing;
using System.Windows.Forms;

namespace Snake.GUI
{
    class GamePanel : Control
    {
        private Engine game;
        private int scale;

        public GamePanel(int width, int height, int scale, Engine game) : base()
        {
            this.game = game;
            this.scale = scale;

            SetStyle(ControlStyles.UserPaint | ControlStyles.AllPaintingInWmPaint | ControlStyles.OptimizedDoubleBuffer, true);
            BackColor = Color.Black;
            Width = width * scale + 1;
            Height = height * scale + 1;

            Paint += GamePanel_Paint;
        }

        private void GamePanel_Paint(object sender, PaintEventArgs e)
        {
            game.Draw(new GraphicsRenderer(e.Graphics, scale));
        }
    }
}
