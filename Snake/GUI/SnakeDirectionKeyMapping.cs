﻿using System.Windows.Forms;

namespace Snake.GUI
{
    class SnakeDirectionKeyMapping
    {
        public Keys Up { get; private set; }
        public Keys Left { get; private set; }
        public Keys Down { get; private set; }
        public Keys Right { get; private set; }
        
        public SnakeDirectionKeyMapping(Keys up, Keys left, Keys down, Keys right)
        {
            Up = up;
            Left = left;
            Down = down;
            Right = right;
        }
    }
}
