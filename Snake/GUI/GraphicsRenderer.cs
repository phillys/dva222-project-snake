﻿using System.Drawing;
using Snake.Interfaces;

namespace Snake.GUI
{
    class GraphicsRenderer : IRenderer
    {
        private Graphics graphics;
        private Pen pen = new Pen(Color.DarkGray);
        private int scale;

        public GraphicsRenderer(Graphics graphics, int scale)
        {
            this.graphics = graphics;
            this.scale = scale;
        }

        public void DrawSquare(Point position, Color color)
        {
            graphics.FillRectangle(new SolidBrush(color), position.X * scale, position.Y * scale, scale, scale);
            graphics.DrawRectangle(pen, position.X * scale, position.Y * scale, scale, scale);
        }
    }
}
