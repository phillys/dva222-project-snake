﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace Snake.GUI
{
    public class MainForm : Form
    {
        private int width = 50;
        private int height = 50;
        private int scale = 20;
        private int FPS = 40;

        private Engine game;
        private GamePanel gamePanel;
        private GameMenu menu;
        private ScorePanel scorePanel;
        private Timer timer = new Timer();
        private SnakeKeyHandler snakeKeyHandler = new SnakeKeyHandler();
        private Label winnerLabel;

        public MainForm() : base()
        {
            AutoSize = true;
            FormBorderStyle = FormBorderStyle.FixedDialog;
            MaximizeBox = false;
            StartPosition = FormStartPosition.CenterScreen;
            Text = "Snake";

            game = new Engine(FPS, width, height);
            game.GameOver += Game_GameOver;

            KeyDown += MainForm_KeyDown;
            KeyPreview = true;

            timer.Interval = 1000 / FPS;
            timer.Tick += Timer_Tick;

            var flow = new FlowLayoutPanel();
            flow.FlowDirection = FlowDirection.LeftToRight;
            flow.AutoSize = true;

            gamePanel = new GamePanel(width, height, scale, game);
            flow.Controls.Add(gamePanel);

            scorePanel = new ScorePanel(height, scale);
            flow.Controls.Add(scorePanel);

            menu = new GameMenu();
            menu.Players1.Click += Players1_Click;
            menu.Players2.Click += Players2_Click;
            menu.Players3.Click += Players3_Click;

            winnerLabel = new Label
            {
                AutoSize = true,
                BackColor = Color.Black,
                Font = new Font("Calibri", 20)
            };
            
            Controls.Add(flow);
            Controls.Add(menu);
            Controls.Add(winnerLabel);

            winnerLabel.Location = new Point(10, 10);
            winnerLabel.BringToFront();
            menu.Location = new Point((gamePanel.Width - menu.Width) / 2, (gamePanel.Height - menu.Height) / 2);
            menu.BringToFront();
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            game.Tick();
            gamePanel.Refresh();
        }

        private void Game_GameOver(IList<Snake> winners)
        {
            timer.Stop();
            menu.Visible = true;
            snakeKeyHandler.Clear();
            
            winnerLabel.Text = $"{String.Join(", ", winners.Select(s => s.Name))} Won!";
            winnerLabel.ForeColor = winners[0].Color;
            winnerLabel.Visible = true;
        }

        private void StartGame(IList<Snake> snakes)
        {
            snakeKeyHandler.Add(snakes);
            scorePanel.SetupScores(snakes);
            menu.Visible = false;
            winnerLabel.Visible = false;
            gamePanel.Focus();
            timer.Start();
        }

        private void Players1_Click(object sender, EventArgs e)
        {
            StartGame(game.AddSnakes());
        }

        private void Players2_Click(object sender, EventArgs e)
        {
            StartGame(game.AddSnakes(2));
        }

        private void Players3_Click(object sender, EventArgs e)
        {
            StartGame(game.AddSnakes(3));
        }

        private void MainForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape && timer.Enabled)
            {
                game.QuitRound();
            }

            snakeKeyHandler.Handle(e.KeyCode);
        }
    }
}
