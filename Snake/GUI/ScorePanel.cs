﻿using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace Snake.GUI
{
    class ScorePanel : UserControl
    {
        private int scale;
        private Font font = new Font("Calibri", 20);
        private FlowLayoutPanel labelsFlow;

        public ScorePanel(int height, int scale) : base()
        {
            this.scale = scale;

            AutoSize = true;
            MinimumSize = new Size(200, height * scale);
            BackColor = Color.FromArgb(0x20, 0x20, 0x20);

            labelsFlow = new FlowLayoutPanel();
            labelsFlow.FlowDirection = FlowDirection.TopDown;
            labelsFlow.AutoSize = true;
            Controls.Add(labelsFlow);
            
            Snake.ScoreChanged += UpdateScore;
        }

        private void UpdateScore(string name, int score)
        {
            var controls = labelsFlow.Controls.Find(name, false);
            if (controls.Length > 0)
            {
                controls[0].Text = $"{name}: {score}";
            }
        }

        public void SetupScores(IEnumerable<Snake> snakes)
        {
            labelsFlow.Controls.Clear();

            foreach (var snake in snakes)
            {
                labelsFlow.Controls.Add(new Label
                {
                    AutoSize = true,
                    Font = font,
                    Left = 10,
                    Name = snake.Name,
                    Text = $"{snake.Name}: {snake.Score}",
                    ForeColor = snake.Color
                });
            }
        }
    }
}
