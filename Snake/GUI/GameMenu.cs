﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Snake.GUI
{
    class GameMenu : FlowLayoutPanel
    {
        public Button Players1 { get; }
        public Button Players2 { get; }
        public Button Players3 { get; }

        public GameMenu() : base()
        {
            AutoSize = true;
            AutoSizeMode = AutoSizeMode.GrowAndShrink;
            FlowDirection = FlowDirection.TopDown;
            
            var exit = new Button() { Text = "Exit" };
            exit.Click += Exit_Click;

            Players1 = new Button() { Text = "1 Player" };
            Players2 = new Button() { Text = "2 Players" };
            Players3 = new Button() { Text = "3 Players" };
            
            Controls.Add(Players1);
            Controls.Add(Players2);
            Controls.Add(Players3);
            Controls.Add(exit);
        }

        private void Exit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
