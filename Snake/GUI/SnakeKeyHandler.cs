﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Snake.GUI
{
    class SnakeKeyHandler
    {
        private IDictionary<Snake, SnakeDirectionKeyMapping> snakeKeyMappings = new Dictionary<Snake, SnakeDirectionKeyMapping>();
        private SnakeDirectionKeyMapping[] keyDirectionMappings = new SnakeDirectionKeyMapping[] {
           new SnakeDirectionKeyMapping(Keys.W, Keys.A, Keys.S, Keys.D),
           new SnakeDirectionKeyMapping(Keys.I, Keys.J, Keys.K, Keys.L),
           new SnakeDirectionKeyMapping(Keys.T, Keys.F, Keys.G, Keys.H)
        };
        private int count = 0;

        public void Add(IEnumerable<Snake> snakes)
        {
            foreach (var snake in snakes)
            {
                snakeKeyMappings.Add(snake, keyDirectionMappings[count++]);
            }
        }

        public void Clear()
        {
            snakeKeyMappings.Clear();
            count = 0;
        }

        public void Handle(Keys key)
        {
            foreach (var mapping in snakeKeyMappings)
            {
                if (key == mapping.Value.Up)
                {
                    mapping.Key.Direction = Snake.Directions.Up;
                    break;
                }
                else if (key == mapping.Value.Down)
                {
                    mapping.Key.Direction = Snake.Directions.Down;
                    break;
                }
                else if (key == mapping.Value.Left)
                {
                    mapping.Key.Direction = Snake.Directions.Left;
                    break;
                }
                else if (key == mapping.Value.Right)
                {
                    mapping.Key.Direction = Snake.Directions.Right;
                    break;
                }
            }
        }
    }
}
