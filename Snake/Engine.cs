﻿using System;
using System.Linq;
using System.Collections.Generic;
using Snake.Foods;
using Snake.Interfaces;

namespace Snake
{
    class Engine
    {
        internal int FPS;
        private int width;
        private int height;
        private IList<ICollidable> collidables = new List<ICollidable>();
        private ICollider collider;
        private Random random = new Random();
        private SnakeFactory snakeFactory;
        private IList<Snake> snakes = new List<Snake>();
        private IList<Snake> deadSnakes = new List<Snake>();
        private Trigger foodSpawnTrigger = new Trigger();

        public delegate void GameOverHandler(IList<Snake> winners);
        public event GameOverHandler GameOver;

        public Engine(int fps, int width, int height)
        {
            FPS = fps;
            collider = new Collider(width, height);
            snakeFactory = new SnakeFactory(this);
            foodSpawnTrigger.ThresholdReached += SpawnRandomFood;
            this.width = width;
            this.height = height;
        }

        public IList<Snake> AddSnakes(int count = 1)
        {
            for (var i = 1; i <= count; i++)
            {
                snakes.Add(
                    snakeFactory.Create(width / (count + 1) * i, height / 4, $"Player {i}")
                );
            }

            return new List<Snake>(snakes);
        }

        public Snake GetRandomSnake()
        {
            return snakes[random.Next(snakes.Count)];
        }

        public void Remove(ICollidable collidable)
        {
            collidables.Remove(collidable);
        }

        public void Remove(Snake snake)
        {
            deadSnakes.Add(snake);
            snakes.Remove(snake);
        }

        public void QuitRound()
        {
            GameOver?.Invoke(GetWinningSnakes());
            snakes = new List<Snake>();
            deadSnakes = new List<Snake>();
            collidables = new List<ICollidable>();
            foodSpawnTrigger.Threshold = 0;
            snakeFactory.Reset();
        }

        public void Draw(IRenderer renderer)
        {
            foreach (var collidable in collidables)
            {
                collidable.Draw(renderer);
            }

            foreach (var snake in snakes)
            {
                snake.Draw(renderer);
            }
        }

        public void Tick()
        {
            if (snakes.Count == 0) QuitRound();
            Collide();
            Animate();
            foodSpawnTrigger.Tick();
        }

        private void Animate()
        {
            foreach (var snake in snakes)
            {
                snake.Tick();
            }
        }

        private void Collide()
        {
            collider.Clear();

            foreach (var collidable in new List<ICollidable>(collidables))
            {
                collider.Collide(collidable);
            }

            foreach (var snake in new List<Snake>(snakes))
            {
                collider.Collide(snake);
            }
        }

        private IList<Snake> GetWinningSnakes()
        {
            return deadSnakes
                .Union(snakes)
                .GroupBy(s => s.Score)
                .OrderByDescending(g => g.Key)
                .First()
                .ToList();
        }

        private void SpawnRandomFood()
        {
            if (collidables.Count < 10)
            {
                var foodType = Food.Types[random.Next(Food.Types.Length)];
                Food food = (Food) Activator.CreateInstance(foodType, random.Next(width), random.Next(height), this);
                collidables.Add(food);
            }

            foodSpawnTrigger.Threshold = random.Next(0, 5) * FPS;
        }
    }
}
