﻿namespace Snake
{
    public class Trigger
    {
        private int threshold;
        private int count;

        public int Threshold
        {
            private get => threshold;
            set
            {
                threshold = (value > 0) ? value : 1;
                count = threshold;
            }
        }

        public delegate void TriggerEventHandler();
        public event TriggerEventHandler ThresholdReached;

        public Trigger(int threshold = 0)
        {
            Threshold = threshold;
        }

        public void Tick()
        {
            if (count == 0)
            {
                count = threshold;
                ThresholdReached?.Invoke();
            }

            count--;
        }
    }
}
