﻿using System.Windows.Forms;
using Snake.GUI;

namespace Snake
{
    static class Program
    {
        static void Main()
        {
            var form = new MainForm();
            Application.Run(form);
        }
    }
}
