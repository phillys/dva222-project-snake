﻿using System.Drawing;

namespace Snake.Foods
{
    class DoubleSpeedFood : Food
    {
        public DoubleSpeedFood(int x, int y, Engine game)
            : base(x, y, Color.Red, game) { }

        public override void OnCollision(SnakeHead snake)
        {
            snake.Eat(this);
        }
    }
}
