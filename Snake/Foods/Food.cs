﻿using System;
using System.Drawing;
using Snake.Interfaces;

namespace Snake.Foods
{
    abstract class Food : ICollidable
    {
        private Color color;
        private Engine game;
        public Point Position { get; private set; }

        public static Type[] Types => new Type[] {
            typeof(DoubleSpeedFood),
            typeof(ValuableFood),
            typeof(StandardFood)
        };

        protected Food(int x, int y, Color color, Engine game)
        {
            Position = new Point(x, y);
            this.color = color;
            this.game = game;
        }

        public void Draw(IRenderer renderer)
        {
            renderer.DrawSquare(Position, color);
        }

        public void Eat()
        {
            game.Remove(this);
        }

        public void OnCollision(SnakeBlock block) { }

        public abstract void OnCollision(SnakeHead head);
    }
}
