﻿using System.Drawing;

namespace Snake.Foods
{
    class StandardFood : Food
    {
        public StandardFood(int x, int y, Engine game)
            : base(x, y, Color.White, game) { }

        public override void OnCollision(SnakeHead snake)
        {
            snake.Eat(this);
        }
    }
}
