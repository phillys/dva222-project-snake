﻿using System.Drawing;

namespace Snake.Foods
{
    class ValuableFood : Food
    {
        public ValuableFood(int x, int y, Engine game)
            : base(x, y, Color.Yellow, game) { }

        public override void OnCollision(SnakeHead snake)
        {
            snake.Eat(this);
        }
    }
}
